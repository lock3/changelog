#pragma once

#include <boost/date_time.hpp>

#include <optional>


namespace lock3::changelogs
{
  /// Represents the author (or applier) of a change. Note that author may
  /// have multiple email addresses.
  struct author
  {
    author() = default;
    author(std::string const& n, std::string const& e)
      : name(n)
    {
      email.push_back(e);
    }

    std::string name;
    std::vector<std::string> email;
  };

  /// A single entry in a change log. Each change has a date, one or more
  /// authors, and its details. Note that older changelog entries have
  /// a time stamp. We record this when we see it and use it to help
  /// order changes.
  struct change
  {
    int line;
    boost::gregorian::date date;
    std::optional<boost::posix_time::time_duration> time;
    std::vector<author> authors;
    std::vector<std::string> details;
  };

  /// Represents a sequence of changes and some trailing text lines.
  class changelog : public std::vector<change>
  {
  public:
    std::vector<std::string> rest;
  };


  // Streaming operators

  std::ostream& operator<<(std::ostream& os, author const& a);
  std::ostream& operator<<(std::ostream& os, change const& ch);
  std::ostream& operator<<(std::ostream& os, changelog const& log);

  // FIXME: It might be better to use parser and generator classes,
  // since we can more easily provide options to parsing and generation
  // operations.
  std::istream& operator>>(std::istream& is, changelog& log);


  // Comparison functions

  /// Used to sort changes in ascending order.
  struct change_less
  {
    bool operator()(change const& a, change const& b) const noexcept
    {
      if (a.date < b.date)
        return true;
      if (b.date < a.date)
        return false;
      return a.time < b.time;
    }
  };

  /// Used to sort changes in descending order.
  struct change_greater
  {
    bool operator()(change const& a, change const& b) const noexcept
    {
      if (a.date > b.date)
        return true;
      if (b.date > a.date)
        return false;
      return a.time > b.time;
    }
  };

} // namespace lock3::changelogs
