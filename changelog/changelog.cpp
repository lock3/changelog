#include "changelog.hpp"

#include <unordered_map>

namespace lock3::changelogs
{

static std::ostream&
error(int num)
{
  return std::cerr << "error (" << num << "): ";
}

static std::ostream&
note(int num)
{
  return std::cerr << "note (" << num << "): ";
}

/// Parse the author and email into the change.
static bool
parse_author(std::string const& str, change& ch, int num)
{
  // Usually, email addresses are delimited by a two-space sequence.
  std::size_t delim = str.find("  ");
  std::size_t start = delim + 2; // Start of address
  if (delim == str.npos) {
    // Sometimes, people only delimit with a single space. Try
    // searching for angles and parens.
    delim = str.find_first_of("<(");
    if (delim == str.npos) {
      error(num) << "no email address in '" << str << "'\n";
      ch.authors.emplace_back(str, "");
      return false;
    }
    start = delim;
    --delim;
  }
  // Sometimes, there are more than two leading spaces.
  while (str[start] == ' ')
    ++start;
  std::size_t end = str.npos; // End of address
  if (str[start] == '<' || str[start] == '(') {
    ++start;
    end = str.find_first_of(">)", start + 1);
  }
  else {
    // FIXME: Control diagnostics through flags.
    // note(num - 1) << "unenclosed email address '"
    //               << str.substr(start, end - start) << "'\n";
  }
  ch.authors.emplace_back(str.substr(0, delim), str.substr(start, end - start));

  // There may be multiple email addresses.
  author& auth = ch.authors.back();
  while (end != str.npos) {
    start = str.find_first_of("<(", end + 1);
    if (start == str.npos)
      break;
    ++start;
    end = str.find_first_of(">)", start + 1);
    auth.email.push_back(str.substr(start, end - start));
  }

  return true;
}

/// Parse the date in extended ISO format: YYYY-MM-DD.
static bool
parse_iso_date(std::string const& str, change& ch, int num)
{
  try {
    ch.date = boost::gregorian::from_string(str.substr(0, 10));
  }
  catch (std::exception& err) {
    error(num) << err.what() << '\n';
    note(num) << "got '" << str.substr(0, 10) << "'\n";
    return false;
  }

  // In older files, we may have added the time of day before
  // the name. Go ahead and preserve that.
  std::size_t start = 12;
  if (str[10] == ' ' && std::isdigit(str[11])) {
    try {
      ch.time = boost::posix_time::duration_from_string(str.substr(11, 8));
    } catch (std::exception& err) {
      error(num) << err.what() << '\n';
      note(num) << "got '" << str.substr(0, 10) << "'\n";
      return false;
    }
    start = 21;
  }

  return parse_author(str.substr(start), ch, num);
}

/// A mapping of month abbreviations to their indexes.
static std::unordered_map<std::string, int> dates {
  {"Jan", 1},
  {"Feb", 2},
  {"Mar", 3},
  {"Apr", 4},
  {"May", 5},
  {"Jun", 6},
  {"Jul", 7},
  {"Aug", 8},
  {"Sep", 9},
  {"Oct", 10},
  {"Nov", 11},
  {"Dec", 12}
};

static int
lookup_month(std::string const& str, int num)
{
  if (dates.find(str) != dates.end())
    return dates.find(str)->second;
  error (num) << "invalid month '" << str << "'\n";
  return 0;
}

// The date is in the format "Weekday Month Day Time Year".
// Format is %a %b %d %f [time] %Y. Note that there aren't any
// straightforward parsers of this so, we'll have to rebuild
// the entire string.
static bool
parse_unusual_date(std::string const& str, change& ch, int num)
{
  // Unpack and rebuild the date.
  int month = lookup_month(str.substr(4, 3), num);
  if (!month)
    return false;

  int day = std::stoi(str.substr(8, 2));

  // The time of day may be followed by 3-letter time zone and 
  // daylight savings time indicator.
  int yearpos = 20;
  if (std::isalpha(str[yearpos])) {
    note(num) << "time of day includes time zone '"
              << str.substr(yearpos, 3) << "'\n";
    yearpos += 4;
    if (std::isalpha(str[yearpos])) {
    note(num) << "time of day includes daylight savings indicator '"
              << str.substr(yearpos, 3) << "'\n";
      yearpos += 4;
    }
  }
  int year = std::stoi(str.substr(yearpos, 4));

  // Unpack and rebuild the time of day.
  // FIXME: If there was a time zone, should we adjust it?
  // FIXME: Can I use duration from string?
  int hours = std::stoi(str.substr(11, 2));
  int minutes = std::stoi(str.substr(14, 2));
  int seconds = std::stoi(str.substr(17, 2));

  try {
    ch.date = boost::gregorian::date(year, month, day);
    ch.time = boost::posix_time::time_duration(hours, minutes, seconds);
  }
  catch (std::exception& err) {
    error(num) << err.what() << '\n';
    note(num) << "got '" << str.substr(0, 24) << "'\n";
    return false;
  }

  return parse_author(str.substr(26), ch, num);
}

/// Parse the header and first author into the change. Returns true
/// if parsing succeeds.
static bool
parse_header(std::string const& str, change& ch, int num)
{
  if (std::isdigit(str[0]))
    return parse_iso_date(str, ch, num);
  else
    return parse_unusual_date(str, ch, num);
}

/// Get one line of text; increments the line number.
static std::istream&
get_line(std::istream& is, std::string& str, int& num)
{
  if (std::getline(is, str))
    ++num;
  return is;
}

/// Get the header of a change.
static std::istream&
get_header(std::istream& is, change& ch, int& num)
{
  // Match the head line containing the date and lead author.
  std::string head;
  if (!get_line(is, head, num))
    return is;
  if (!parse_header(head, ch, num)) {
    is.setstate(std::ios_base::failbit);
    return is;
  }

  // Consume multiple author lines.
  while (is.peek() == '\t') {
    // Sometimes people put changes immediately after the header line.
    // And sometimes the empty line is a tab. In either case roll back
    // the first character and treat the line as a detail.
    char c1 = is.get();
    if (is.peek() == '*')
      return is.unget();
    if (is.peek() == '\n')
      return is.unget();

    // Otherwise, this is an author line.
    std::string author;
    get_line(is, author, num);
    int start = author.find_first_not_of(" \t");
    parse_author(author.substr(start), ch, num);
  }
  return is;
}

/// True if the string is empty.
static bool
is_blank(std::string const& str)
{
  if (str.empty())
    return true;
  return str.find_first_not_of(" \t") == str.npos;
}

/// Ensure that we don't have duplicate whitespace in the details
/// of the change. We tend to generate extra lines when they aren't
/// written in their canonical form.
static void
normalize(change& ch)
{
  return;
  auto equal_if_blank = [](std::string const& a, std::string const& b) {
    return is_blank(a) && is_blank(b);
  };
  auto iter = std::unique(ch.details.begin(), ch.details.end(), equal_if_blank);
  ch.details.erase(iter, ch.details.end());
}

/// Get the details of a change.
static std::istream&
get_details(std::istream& is, change& ch, int& num)
{
  while (is) {
    char c = is.peek();

    // A line starting with a form feed denotes the end of the file.
    if (c == 0x0c)
      break;

    // A line starting with an alphanumeric character starts a new change.
    if (std::isalnum(c))
      break;

    // Otherwise, we have an empty line or an indented content line.
    // FIXME: Warn if indenting with spaces? It happens.

    // Fetch and accumulate the details.
    std::string line;
    if (!get_line(is, line, num))
      break;

    // Guarantee that the first line is empty, so that we can
    // normalize output.
    if (ch.details.empty() && !line.empty()) {
      // FIXME: Control diagnostics through flags.
      // note(num - 1) << "non-empty line after header\n";
      ch.details.emplace_back();
    }

    ch.details.push_back(line);
  }

  // Guarantee the last line is empty.
  if (!ch.details.back().empty()) {
    // FIXME: Control diagnostics through flags.
    // note(num - 1) << "non-empty line after after change\n";
    ch.details.emplace_back();
  }

  // Fix redundant lines.
  normalize(ch);

  return is;
}

/// Get a single change.
static std::istream&
get_change(std::istream& is, change& ch, int& num)
{
  ch.line = num;
  if (!get_header(is, ch, num))
    return is;
  return get_details(is, ch, num);
}

/// Get all changes.
static std::istream&
get_changes(std::istream& is, changelog& log, int& num)
{
  while (is && is.peek() != 0x0c) {
    change ch;
    if (!get_change(is, ch, num))
      break;
    log.push_back(ch);
  }
  return is;
}

/// Get the trailer of the changelog.
static std::istream&
get_rest(std::istream& is, changelog& log, int& num)
{
  assert(is.peek() == 0x0c);
  is.ignore();
  while (is) {
    std::string line;
    if (!get_line(is, line, num))
      break;
    log.rest.push_back(line);
  }
  return is;
}

std::ostream&
operator<<(std::ostream& os, author const& auth)
{
  os << auth.name << "  ";
  for (auto iter = auth.email.begin(); iter != auth.email.end(); ++iter) {
    os << '<' << *iter << '>';
    if (std::next(iter) != auth.email.end())
      os << ' ';
  }
  return os;
}

std::ostream&
operator<<(std::ostream& os, change const& ch)
{
  // Write the header and authors.
  auto iter = ch.authors.begin();
  os << ch.date;
  if (ch.time)
    os << ' ' << *ch.time;
  os << "  " << *iter++ << '\n';
  while (iter != ch.authors.end())
    os << "\t\t" << *iter++ << '\n';

  // Write the contents.
  for (std::string const& str : ch.details)
    os << str << '\n';

  return os;
}

std::ostream&
operator<<(std::ostream& os, changelog const& log)
{
  for (change const& ch : log)
    os << ch;
  os.put(0x0c);
  for (std::string const& str : log.rest)
    os << str << '\n';
  return os;
}

std::istream&
operator>>(std::istream& is, changelog& log)
{
  int num = 1;
  if (!get_changes(is, log, num))
    return is;
  return get_rest(is, log, num);
}

} // namespace lock3::changelogs
