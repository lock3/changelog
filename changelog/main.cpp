#include "changelog.hpp"

#include <boost/filesystem.hpp>

#include <cstdlib>
#include <cctype>
#include <iostream>
#include <fstream>

using namespace lock3::changelogs;


/// Encapsulates the choice of using cin or an input file.
struct input_source
{
  input_source(char const* input)
    : is(nullptr)
  {
    if (!std::strcmp(input, "-")) {
      is = &std::cin;
      return;
    }

    ifs.open(input);
    if (!ifs)
      return;
    is = &ifs;
  }

  /// Converts to true if the stream is valid and in a good state.
  explicit operator bool() const
  {
    return is != nullptr && is->good();
  }

  /// Returns the input stream.
  std::istream& stream()
  {
    return *is;
  }

  std::istream* is;
  std::ifstream ifs;
};


static int
check(int argc, char* argv[])
{
  if (argc == 2) {
    std::cerr << "error: missing input file(s)\n";
    std::cerr << "usage: changelog check input-files\n";
    return EXIT_FAILURE;
  }

  input_source src(argv[2]);
  if (!src) {
    std::cerr << "error: could not open input file '" << argv[2] << "'\n";
    return EXIT_FAILURE;
  }

  changelog log;
  src.stream() >> log;
  if (!src.stream().eof())
    return EXIT_FAILURE;

  // Note out of order entries.
  change_greater cmp;
  auto iter = log.begin();
  while (iter != log.end()) {
    iter = std::is_sorted_until(iter, log.end(), cmp);
    if (iter != log.end()) {
      std::cerr << "note (" << iter->line <<"): entry at " << iter->date
                << " (" << iter->authors.front() << ") "
                << "is out of order\n";
    }
  }
  return EXIT_SUCCESS;
}

static int
sort(int argc, char* argv[])
{
  if (argc == 2) {
    std::cerr << "error: missing input file\n";
    std::cerr << "usage: changelog sort input-file\n";
    return EXIT_FAILURE;
  }

  input_source src(argv[2]);
  if (!src) {
    std::cerr << "error: could not open input file '" << argv[2] << "'\n";
    return EXIT_FAILURE;
  }

  changelog log;
  src.stream() >> log;
  if (!src.stream().eof())
    return EXIT_FAILURE;

  // Sort in ascending order.
  change_greater cmp;
  std::stable_sort(log.begin(), log.end(), cmp);

  // Write out the log.
  std::cout << log << '\n';

  return EXIT_SUCCESS;
}

static int
invalid(std::string const& cmd)
{
  std::cerr << "error: invalid command '" << cmd << "'\n";
  return EXIT_FAILURE;
}

int
main(int argc, char* argv[])
{
  if (argc == 1) {
    std::cerr << "error: missing command\n\n";
    std::cerr << "usage: changelog [command] [options]\n";
    return EXIT_FAILURE;
  }

  // Print dates in extended ISO format.
  auto* facet = new boost::gregorian::date_facet("%Y-%m-%d");
  std::cerr.imbue(std::locale(std::cout.getloc(), facet));
  std::cout.imbue(std::locale(std::cout.getloc(), facet));

  std::string cmd = argv[1];
  if (cmd == "check")
    return check(argc, argv);
  if (cmd == "sort")
    return sort(argc, argv);
  return invalid(cmd);
}
